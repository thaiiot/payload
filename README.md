# 1 Subsystem Introduction

The main mission of THAIIOT payload system is Store and Forward which gathers low-speed (40-300 bps) data from small ground-based transmitters (End devices) as the satellite passes. Currently, there are many satellites with the Store and Forward mission, such as ADEOS-II (JAXA) and OG2 (ORBCOMM), but due to several limitations such as expensive transceivers, large size antenna, high power consumption and some were allowed to use only for research. Therefore, the mission of this project aims to develop a satellite that provides the Store and Forward service with a small form-factor transmitter, low cost and easy to use, so general users can take full advantage from this project.

![]( The_Store_and_Forward_system_for_small_satellites.png) 

**Figure 1: The Store and Forward system for small satellites**

The application mainly focuses on the areas that telephone network or Internet cannot be reached. The data that collected by the satellite should be data from sensors such as water level sensors, water quality sensor, temperature sensor used in agriculture which contain small size of data. When the satellite approach to the ground station, the information collected in the satellite&#39;s memory will be sent down to the ground station. As shown in Figure 1.

In conclusion, the mission objectives can be summarized as follows

1. Applied in terrain where ground communication is inaccessible, such as forest, seas, valleys as well as in natural disaster areas where conventional ground communication is not available
2. Able to provide IoT services covering all areas of Thailand
3. Ground node devices can transmit long-range data to satellites in Low Earth Orbit (LEO)
4. Low power consumption Ground node devices device which can last about 1-2 years when powered by a small size battery
5. Satellites can receive data from multiple Ground node devices at the same time to cover the widest area
6. Use a technology for long distance communication using low power (Low Power Wide Area Network, LPWAN)

# 2 Subsystem Requirement

To achieve the objectives described in the previous section. A summary of important capabilities that a payload system must have will be described in this section.

## 2.1 Function of the payload

The payload module of THAIIOT satellite may have these capabilities to satisfy the Store and Forward objectives

- Has the ability to communicate at 500 - 2000 kilometers due to the satellite will be on the Low Earth Orbit (LEO) orbit
- Able to receive the data from at least 10 Ground node devices during the mission
- The amount of data that can be received from the Ground node devices in one message must not be less than 50 bytes, to be sufficient for data of various sensors installed on the Ground node devices.
- The transmission speed must be more than 1 bit per second (bps)
- Lifetime of both the payload system on satellite and the Ground node devices must be more than 1 year

## 2.2 Analysis of the payload system characteristics

In order to meet the required store and forward mission of the THAIIOT cubesat, the Low Power Wide Area Network (LPWAN) is selected due to its low power consumption and wide area coverage [1]. LPWAN technology has an important role in the connectivity of devices with small amount of the transmitted data. It can operate continuously with very little power consumption which is suitable for the IoT application. Moreover, LPWAN can also extend the range of the communication. The advantages of LPWAN over other communication methods in terms of range and power consumption are illustrated in Figure 2.

![](Comparison_of_communication_methods.jpg)

**Figure 2: Comparison of communication methods [2]**

LPWAN technology has been developed for the commercial purpose by several companies such as Long Term Evolution for Machine Type Commutation (LTE-M), Narrow Band IoT (NB-IoT), SigFox (SigFox), and Long Range (LoRa). All of the mentioned technology are capable of long range communication and consume less energy. However, each company has different approach technique to operate the LPWAN depending on the business model.

Table 1 shows the comparison of each technology [3]. From Table 1, it shows several advantages of LoRaWAN over other technologies as follows.

- Data rate is ranging from 290 bps to 50 kbps at 125 kHz which requires shorter Time-on-Air

- It consumes the least energy

- The Chirp Spread Spectrum (CSS) modulation can transmit at the level lower than noise floor

- The data can be encrypted both on public and private network

- The data rate is adaptive

- LoRaWAN can be use freely without any subscription

From all above advantages, LoRaWAN is selected as the technology to develop the payload gateway and end devices for THAIIOT project.

| **Feature** | **LoRaWAN** | **Sigfox** | **NB-IoT** | **LTE-M** |
| --- | --- | --- | --- | --- |
| Modulation | SS Chirp | GFSK/DBPSK | UNB/GFSK/BPSK | OFDMA |
| Data Rate | 290 bps – 50 kbps | 100 bps
 12/8 bytes Max | 100 bps
 12/8 bytes Max | 200 kbps – 1 Mbps |
| Link Budget | 154 dB | 146 dB | 151 dB | 146 dB |
| Battery lifetime | 8 – 10 years | 7 – 8 years | 7 – 8 years | 1 – 2 years |
| Power Efficiency | Very High | Very High | Very High | Medium |
| Security/Authentication | Yes (32 bits) | Yes (16 bits) | No | Yes (32 bits) |
| Range | 2 – 5 km urban
 15 km suburban
 45 km rural | 3 – 10 km urban
 -
 30 – 50 km rural | 1.5 km urban
 -
 20 – 40 km rural | 35 km – 2G
 200 km – 3G
 200 km – 4G |
| Interference Immunity | Very High | Low | Low | Medium |
| Scalability | Yes | Yes | Yes | Yes |
| Mobility/Localization | Yes | No | Limited, No Loc | Only Mobility |

**Table 1: Comparison of LPWAN technology [4]**

# 3 Subsystem Overview and Specification

Based on the above literature review, this project chose LoRa as the satellite LPWAN technology. The LoRa module that has been selected is LoRa SX1301 Class B. Because it has the ability to receive 8 channels in a single module, which saves space and energy, and has the appropriate frequency range, Spreading Factor, Bandwidth and Est. Sensitivity values ​​for the mission. For the nodes on the ground (End device), the LoRa SX1278 module will be used because it covers the frequency band 436 - 438 MHz, which the NBTC has allocated this band for IoT devices, which has been allowed to use in Thailand without special license, and it also has a low Est. Sensitivity, which is good for long range communication. Detailed information on comparing the parameter of different types of LoRa modules can be found at Table 2

| **Part Number** | **Frequency Range**** (MHz) **|** Spreading Factor **|** Bandwidth****(kHz)** | **Effective Bitrate**** (kbps) **|** Est. Sensitivity****(dBm)** |
| --- | --- | --- | --- | --- | --- |
| SX1276 | 137 - 1020 | 6-12 | 7.8 - 500 | 0.018 – 37.5 | -111 to -148 |
| SX1277 | 137 - 1020 | 6-9 | 7.8 - 500 | 0.011 – 37.5 | -111 to -148 |
| SX1278 | 137 - 525 | 6-12 | 7.8 - 500 | 0.018 – 37.5 | -111 to -139 |
| SX1279 | 137 - 960 | 6-12 | 7.8 - 500 | 0.018 – 37.5 | -111 to -148 |
| SX1301 | 429 - 928 | 6-12 | 125/250/500 | 0.018 – 37.5 | -111 to -142.5 |

**Table**  **2 SX1276/77/78/79 and SX1301 Device Variants and Key Parameters**

In addition, this project chose 8-channel transmitter as it allows for higher volumes of data transmission. Although Thailand has been allocated the most frequencies that can be used with the LoRa system is 10 channels. but in Uplink, there are only 8 channels that meet the needs of the work of this project.

The maximum spreading factor is 12 because the spreading factor affects the transmission distance, Table 3 show the limitation of the LoRa Channel and the frequency plan that can be used in Thailand.

**Uplink:** 
| Ch. | Frequency | SF &amp; BW |
| ------ | ------ | ------ |
| 1 | 436.175 | SF7BW125 to SF12BW125 |
| 2 | 436.375 | SF7BW125 to SF12BW125 |
| 3 | 436.575 | SF7BW125 to SF12BW125 |
| 4 | 436.775 | SF7BW125 to SF12BW125 |
| 5 | 436.975 | SF7BW125 to SF12BW125 |
| 6 | 437.175 | SF7BW125 to SF12BW125 |
| 7 | 437.375 | SF7BW125 to SF12BW125 |
| 8 | 437.575 | SF7BW125 to SF12BW125 |


**Downlink:**
| ------ |
| Uplink Ch. 1-8 (RX1) |

**Table 3 LoRa Frequency Plan**

The next topics is to analyze the characteristics of the payload system based on the communication capability when using LoRa technology. The details are as follows.

## 3.1 Sensors and Data

Table 4 shows some examples of sensors and the amount of data it uses as examples of application to project missions.

| **Sensor** | **Unit** | **Bytes, Bits** |
| --- | --- | --- |
| Thermo/Hydro | Celsius, Percent | 4 Bytes |
| Rain | mm | 3 Bytes |
| UV Index | MED/h | 5 Bytes |
| Soil Moisture | dS/m | 12 Bits |
| Wind Speed | MPH | 1 Byte |
| Wind Direction | Azimuth degrees | 1 Byte |
| IoT Network for Air Quality Monitoring | Percent (Quality) | 8 Bytes |

**Table 4 examples of sensors and the amount of data [21]**

## 3.2 Number of Sensors for Coverage Area

According to data of sensor in 3.2 to cover the internet inaccessible area of ​​Thailand, Table 5 shows the coverage area of each type of sensor.

| **Sensor** | **Sensor&#39;s Radius**** (km) **|** Sensor&#39;s coverage Areas (km2) **|** Number of Sensors** |
| --- | --- | --- | --- |
| Thermo/Hydro | 1 | 3.14286 | 96,350 |
| Rain | 1 | 3.14286 | 96,350 |
| UV Index | 1 | 3.14286 | 96,350 |
| Soil Moisture | 0.5 | 0.78571 | 385,217 |
| Wind Speed | 1 | 3.14286 | 96,350 |
| Wind Direction | 0.5 | 0.78571 | 385,217 |
| IoT Network for Air Quality Monitoring | 0.5 | 0.78571 | 385,217 |

**Table 5 Data of coverage area of each sensors [21] [1] [1]**
