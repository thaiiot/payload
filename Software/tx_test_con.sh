#!/bin/bash

#Restart the concentrator on GPIO pin 25
cd /home/pi/lora_gateway/
./reset_lgw.sh start 25

#Start the gateway tx test freq. 433 bandwidth 125 khz sf 12 coding rate 1 packet size 12 byte 5 loop
cd /home/pi/lora_gateway/util_tx_test/
./util_tx_test -r 1255 -f 433 -m LORA -b 125 -s 12 -c 1 -z 12 -p 27 
