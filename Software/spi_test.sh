#!/bin/bash

#Restart the concentrator on GPIO pin 25
cd /home/pi/lora_gateway/
./reset_lgw.sh start 25

#Start the connection test
cd /home/pi/lora_gateway/util_spi_stress/
./util_spi_stress