#!/bin/bash

#Restart the concentrator on GPIO pin 25
cd /home/pi/lora_gateway/
./reset_lgw.sh start 25

#Start the LoRa packet forwarder
cd /home/pi/packet_forwarder/util_pkt_logger/
./util_pkt_logger
